"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import os
import sys
from setuptools import setup, find_packages
import msegtools.__version__ as v

installable_data_files = [
    ('/usr/share/mseg-tools/templates/algorithm-skeleton-cpp', [
        'templates/algorithm-skeleton-cpp/algorithm.cpp.tmpl',
        'templates/algorithm-skeleton-cpp/algorithm.h.tmpl',
        'templates/algorithm-skeleton-cpp/main.cpp.tmpl',
        'templates/algorithm-skeleton-cpp/CMakeLists.txt.tmpl'
    ]),
    ('/usr/share/mseg-tools/templates/algorithm-skeleton-java', [
        'templates/algorithm-skeleton-java/algorithm.java.tmpl',
        'templates/algorithm-skeleton-java/build.xml.tmpl'
    ]),
    ('/usr/share/mseg-tools/templates/algorithm-skeleton-matlab', [
        'templates/algorithm-skeleton-matlab/constructor.m.tmpl',
        'templates/algorithm-skeleton-matlab/reset_training.m.tmpl',
        'templates/algorithm-skeleton-matlab/segment.m.tmpl',
        'templates/algorithm-skeleton-matlab/train.m.tmpl',
        'templates/algorithm-skeleton-matlab/main.tmpl',
        'templates/algorithm-skeleton-matlab/setup.m.tmpl'
    ]),
    ('/usr/share/mseg-tools/templates/algorithm-skeleton-python', [
        'templates/algorithm-skeleton-python/algorithm.py.tmpl'
    ])
]

# If this is a user installation from source, omit installable_data_files
# and fetch templates from source 
if 'install' in sys.argv and '--user' in sys.argv and os.environ.get('MSEG_TOOLS_DIR') is not None:
    installable_data_files = []

setup(
    name="mseg-tools",
    version=v.msegtools_version,
    description="Terminal tools for MSeg",
    author="Christian R. G. Dreher",
    author_email="christian.dreher@student.kit.edu",
    url="https://gitlab.com/h2t/kit-mseg/mseg-tools",
    license='GPL2',
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    data_files=installable_data_files,
    entry_points={'console_scripts': [
        'msegcm = msegtools:msegcm_main',
        'msegdata = msegtools:msegdata_main',
        'mseggen = msegtools:mseggen_main'
    ]},
)
